/*
 * M501.java
 * 
 * Created on 06-mar-2008, 12:13:40
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *
 * @author jose
 */
public class M507 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short linea;
    public short barquetas_Detec;

    // 0- Normal 1- Alarma  3- Normal no actualizado 4- Alarma no actualizado
    public int[] A = new int[16];
// -----
    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      // Linea 1
      linea =  C.BytetoShort(Corte(2));  
      barquetas_Detec =  C.BytetoShort(Corte(2));     

      Concad();
     
   }
  
   private int Estado_Alarma(int int_estado,int A,int posicion,Conv C){
       
       // Se ha producido un estado de alarma
       if(C.IsActive(int_estado,posicion)){
           if(A == 0) A = 3;  // Se activa la alarma
       }
       else{
           if(A == 1) A = 2;  // Se desactiva la alarma
       }
       return A;
   }
   
   // -------------------------------------------
   private void Concad(){
        // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+")b.QS ("+barquetas_Detec+")\n";        
    
   }
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
     private byte Corte_Byte(){
//        byte bt = new byte[x];
        
           byte bt = buffer[I]; I++;
        
        return bt;
    } 
}
