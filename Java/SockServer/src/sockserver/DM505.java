/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M505;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.*;
import java.io.*;
/**
 *
 * @author jose
 */
public class DM505 extends M505{
   DM505(byte[] buffer,Datos DB){

   /*    
     // Borrar los registros del plc
     try{
       PreparedStatement D = DB.prepara_comando("DELETE FROM plc");
       D.execute();
       D.close();
     }catch(SQLException e) {
                                System.out.println("Error borrado información plc");
                                e.printStackTrace();                           
            }
     */
     
     try{  
      decode(buffer);
      
      Conversion X = new Conversion();
      String Cad="INSERT INTO plc (linea,mensaje,codigo,pendiente,cantidad,lote,amasado) VALUES (?,?,?,?,?,?,?)";
      PreparedStatement C = DB.prepara_comando(Cad);
      C.setInt(1,linea);
      if(mensaje > 0) C.setInt(2,mensaje + 360000);  //TEST 22/08/12
      else  C.setInt(2,mensaje);
            
//28052009      C.setInt(2,mensaje);
      C.setInt(3,codigo);
      C.setInt(4,pendiente);      
      C.setInt(5,cantidad);
      C.setInt(6,lote);
      if(amasado > 0) C.setInt(7,amasado + 360000);    //TEST 22/08/12
      else C.setInt(7,amasado);
      
//28052009      C.setInt(7,amasado);  

      C.execute();
      C.close();
     }catch(SQLException e) {
                                System.out.println("Error inserción información plc");
                                e.printStackTrace();
                           //     return 0;
            }
   }    
}
