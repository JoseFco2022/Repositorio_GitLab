
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


import java.io.*;
/**
 *
 * @author jose
 */
public class Config {
    
   public String ruta_base; 
   public String user;
   public String password;
   public boolean grabacion;
   public int ciclo_grabacion;
   public boolean silencio;
   
   public  Config(){
     grabacion = false;
     silencio = false;
     FileReader fr = null;
     FileWriter fi = null;       
     try{  
       File fichero = new File("config.dat");
       if(fichero.exists()){
       fr = new FileReader("config.dat");
       BufferedReader br = new BufferedReader(fr);           
       ruta_base=br.readLine();
       user=br.readLine(); 
       password=br.readLine();
       br.close();
       fr.close(); 
       
       }
       else{       
            ruta_base="jdbc:sqlserver://localhost:1130;Database=DATOS";
            user="SYDBA";
            password="masterkey";
            
            System.out.println("No existe el fichero de Configuración");
       }
       
       

     }catch(IOException e) {
                            System.out.println("No se pudo leer el fichero.");
                            e.printStackTrace();
                            return;
                              }
   }
   
   public void graba_config(){
       FileWriter fi = null;
       try{
          fi = new FileWriter("config.dat");
          PrintWriter pw = new PrintWriter(fi);
          pw.println(ruta_base);           
          pw.println(user);          
          pw.println(password);          
          pw.close();
          fi.close();    
       }catch(IOException e) {
                            System.out.println("No se pudo leer el fichero de config.");
                            e.printStackTrace();
                            return;
                              } 
   }
   

}
