/*
 *  M552 ENVIO DATOS DEL LOTE DE AMASADO
 * 
 */

package sockserver;


/**
 *
 * @author Jose
 */
public class M552 {
    private byte[] buffer;
    private int I = 0;
    
    public short cabecera;
    public short telegrama;    
    public short producto;    
    public String amasado;
    public short barquetas;
    public short peso_masa;
    public short linea;
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      telegrama =  C.BytetoShort(Corte(2));     
      producto =   C.BytetoShort(Corte(2));
      amasado = C.BytetoStr(Corte(4));
      barquetas =  C.BytetoShort(Corte(2));
      peso_masa =  C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));
      
      Concad();
      // Mensaje 
   }
   
   public byte[] encode(){
      Conv C = new Conv();
      C.AddShort(cabecera);
      C.AddShort(telegrama);
      C.AddShort(producto);
      C.AddString(amasado,4);
      C.AddShort(barquetas);
      C.AddShort(peso_masa);      
      C.AddShort(linea);
      buffer = C.getBuffer();
      Concad();  
    
      return buffer;      
   }
    
   private void Concad(){
      MenStr="Mensaje ("+cabecera+")telegrama ("+telegrama+") Producto ("+producto+" amasado ("+amasado;
      MenStr=MenStr+") Barquetas ("+barquetas+") peso masa ("+peso_masa+") linea ("+linea+")\n";                   
   }
   
   
   
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
        

}
