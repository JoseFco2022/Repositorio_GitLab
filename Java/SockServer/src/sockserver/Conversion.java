package sockserver;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Conversion {

    public Conversion () {
    }

    // ------------------------------------------------------------------
    //  CONVIERTE CADENA A DateSql
    // ------------------------------------------------------------------
    public java.sql.Date strDatesql(String Cadfecha)
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");  
      java.sql.Date fechasql = null;
      
      try {
          java.util.Date fecha = sdf.parse(Cadfecha);
          fechasql = new java.sql.Date(fecha.getTime());
      }catch(Exception ex) { ex.printStackTrace();} 
       
      
//     Date fecha = 
      return fechasql;
    }
    // ------------------------------------------------------------------
    //  DEVUELVE LA FECHA ACTUAL
    // ------------------------------------------------------------------
     public java.sql.Date FechaActual()
    {
      SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");  
      java.sql.Date fechasql = null;
      
      try {
          java.util.Date fecha = new Date();
          fechasql = new java.sql.Date(fecha.getTime());
      }catch(Exception ex) { ex.printStackTrace();} 
       
      
//     Date fecha = 
      return fechasql;
    }
    // ------------------------------------------------------------------
    //  CONVIERTE CADENA A INTEGER
    // ------------------------------------------------------------------
    public Integer strInt(String Cadnumero)
    {
      Integer res = 0;
      
      try {
          Cadnumero=Cadnumero.replace(" ","");  
          if(Cadnumero.length()> 0) 
            res = Integer.parseInt(Cadnumero);  
          else
            res = 0;  
//          int numero=Integer.parseInt(Cadnumero.trim());          
      }catch(Exception ex) {ex.printStackTrace();} 

      return res;
    }    
    
        
    // ------------------------------------------------------------------
    //  CONVIERTE CADENA A DOUBLE
    // ------------------------------------------------------------------
    public Double strDouble(String Cadnumero)
    {
      Double res = 0.0;
      
      try {
          Cadnumero=Cadnumero.replace(" ","");          
          res = Double.parseDouble(Cadnumero);  
//          double numero=Double.valueOf(Cadnumero).doubleValue();
      }catch(Exception ex) {ex.printStackTrace();} 

      return res;
    }  
}

