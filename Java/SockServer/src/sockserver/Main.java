/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

// import java.sql.ResultSet;
// import java.sql.*;
import java.net.*;
import java.io.*;
/**
 *
 * @author jose
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {                
       // Conexión de Socket 
        ServerSocket serverSocket = null;
        boolean listening = true;
        // Lineas de produccion
        Lineas L1 = new Lineas();
        Lineas L2 = new Lineas();
        Lineas L3 = new Lineas();
        Lineas L4 = new Lineas();
        // Clientes Comunicacion
        ClienteCom Com = new ClienteCom();
        // Configuracion
        Config Confg = new Config();
        
        // Si se pasa el parametro -t hace un test de conectividad
        if(args.length > 0){
            if(args[0].compareTo("-h") == 0 ){
                     System.out.println("-t : comprobar conexión con SQLSERVER");
                     System.out.println("-g xx : grabar los datos recibidos a un fichero log, cada xx ciclos. ");
                     System.out.println("-s : modo silencioso. ");
                     System.out.println("  -d (jdbc:sqlserver://localhost:1130;Database=DATOS)");
                     System.out.println("  -u (SYSDBA)");
                     System.out.println("  -p (masterkey)");
                     System.exit(1);}
            if(args[0].compareTo("-t") == 0 ){
                System.out.println("Comprobando conexión con SQLSERVER");
                System.out.println("Ruta : "+Confg.ruta_base);
                System.out.println("User : "+Confg.user);
                System.out.println("Password : "+Confg.password);
                Datos DB = new Datos("sqlserver",Confg.ruta_base,Confg.user,Confg.password);                
                System.exit(1);
            }
         if(args[0].compareTo("-g") == 0 ){
                Confg.grabacion = true;  
                Confg.ciclo_grabacion = Integer.parseInt(args[1]);
            }
            
         if(args[0].compareTo("-s") == 0 ){
                Confg.silencio = true;  
            }
            
            if(args[0].compareTo("-c") == 0 ){
                for(int I = 0; I < args.length; I++){
                    if(args[I].compareTo("-d") == 0 ){
                        I++; Confg.ruta_base=args[I];                        
                    }
                    if(args[I].compareTo("-u") == 0 ){
                        I++; Confg.user=args[I];                        
                    }                    
                    if(args[I].compareTo("-p") == 0 ){
                        I++; Confg.password=args[I];                        
                    }                    
                    
                    // Grabación de datos
                    Confg.graba_config();
                    
                }
             System.exit(1); 
            }
            
        }
        
        
        // Apertura de Socket
        try {
         //   serverSocket = new ServerSocket(11000);
            serverSocket = new ServerSocket(11000);
      //     serverSocket.setSoTimeout(15000);
        } catch (IOException e) {
            System.out.println("Error apertura puerto: " + 11000 + ", " + e);
            System.exit(1);
        }
       
      // Bucle para la conexión de multiples clientes  
         while (listening) {
           Socket clientSocket = null;
           try {
             clientSocket = serverSocket.accept();
             System.out.println("v.11.8.Conexión desde "+clientSocket.toString());
            } catch (IOException e) {
             System.err.println("Accept failed: " + 11000 + ", " + e.getMessage());
             continue;
            }
            new MutiSocket(clientSocket,L1,L2,L3,L4,Confg).start();
        //    new MultiClient(clientSocket).start();
           }
 
         try {
            serverSocket.close();
          } catch (IOException e) {
            System.err.println("Could not close server socket." + e.getMessage());
         }        
        
       
       
    }

}
