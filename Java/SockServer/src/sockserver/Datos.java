/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import java.net.*;
import java.io.*;
import java.sql.*;
/**
 *
 * @author jose
 */
class Datos extends BaseDatos {
  
   // Cargar la DB 
   public Datos(String base, String _url, String _login, String _passwd)
   {
       super(base,_url, _login, _passwd);
       Open();
   }
   
   // Envia todos los mensajes pendientes
//   public void Envio_mensajes(PrintStream os)           
 //  public void Envio_mensajes(OutputStream os)           
     public void Envio_mensajes(OutputStream salida)           
   {
       String Cabecera;
       try{
        ResultSet Q=ejecuta_consulta("SELECT * FROM mensaje where estado is null");
            
        // Mostrar los datos
        while ( Q.next()){
            Cabecera=Q.getString("Cabecera");
            
            
            // Tipo de mensaje
            if(Cabecera.equals("551"))  // Mensaje de Pedido
           {
//                ResultSet C=ejecuta_consulta("SELECT  p.*,a.ean,a.etiqueta,a.barquetas_caja,a.peso_barqueta FROM pedido p INNER JOIN artic a on(a.codigo=p.codigo)  WHERE p.mensaje ="+Q.getString("mensaje"));  
                 ResultSet C=ejecuta_consulta("SELECT * FROM v_551  WHERE mensaje ="+Q.getString("mensaje"));  
                 DM551 M = new DM551();
                 M.CreaMensaje(C);
                 if(M.MenStr != null) System.out.printf(M.MenStr);
                 envio(salida,M.buffer);
                 informe(M.cabecera,M.linea,M.MenStr);
                 
            }
            
            
            if(Cabecera.equals("552"))  // Mensaje de Masa
            {
                 ResultSet C2=ejecuta_consulta("select * from v_masa WHERE mensaje ="+Q.getString("mensaje"));  
                 DM552 M2 = new DM552();
                 M2.CreaMensaje(C2);
                 if(M2.MenStr != null) System.out.printf(M2.MenStr);
                 envio(salida,M2.buffer);
                 informe(M2.cabecera,M2.linea,M2.MenStr);
            }            

            if(Cabecera.equals("553"))  // Mensaje de Accion  1-Marcha/2-Paro
            {
                 ResultSet C3=ejecuta_consulta("select * from accion WHERE mensaje ="+Q.getString("mensaje"));  
                 DM553 M3 = new DM553();
                 M3.CreaMensaje(C3);
                 if(M3.MenStr != null) System.out.printf(M3.MenStr);
                 envio(salida,M3.buffer);                 
                 informe(M3.cabecera,M3.linea,M3.MenStr);
            }
            
            if(Cabecera.equals("554"))  // Mensaje de Accion  sobre mensaje del PLC
            {
                 ResultSet C4=ejecuta_consulta("select * from accion WHERE mensaje ="+Q.getString("mensaje"));  
                 DM554 M4 = new DM554();
                 M4.CreaMensaje(C4);
                 if(M4.MenStr != null) System.out.printf(M4.MenStr);
                 envio(salida,M4.buffer);   
                 informe(M4.cabecera,M4.linea,M4.MenStr);
            }            
            
            // Marcar el mensaje como enviado
            comando("UPDATE mensaje set estado = 'E' WHERE mensaje = "+Q.getString("mensaje"));
            
            System.out.println(Q.getString("Mensaje")+" "+Q.getString("Cabecera"));                      
          }
        
       } catch(SQLException e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
       
   }
   // -----------------------------------------------------------------
   //   ULTIMO ELEMENTO
   // -----------------------------------------------------------------
   public int ultimo(String tabla,String Campo)
   {
    int C = 1;  
     try { 
       ResultSet Q = ejecuta_consulta("select max("+Campo+") as C from "+tabla);

       if(Q.next()){            
        C =Q.getInt("C");
        C+=1;
       }               
       Q.close();
     } catch(SQLException e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
       catch(Exception e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
     
       return C;
           
   }
   
      // -----------------------------------------------------------------
   //   ULTIMO ELEMENTO
   // -----------------------------------------------------------------
   public void informe(int cabecera,int linea, String descrip)
   {
    
       /*
     try { 
       int id = ultimo("log","id");   
       String Cad ="INSERT INTO log (id,cabecera,linea,descrip) VALUES (?,?,?,?)";
       PreparedStatement C = prepara_comando(Cad);
       C.setInt(1,id);       
       C.setInt(2,cabecera);
       C.setInt(3,linea);
       if (descrip.length() > 200) 
          C.setString(4,descrip.substring(1,199));
       else 
          C.setString(4,descrip);
       
       
       C.executeUpdate();
       C.close();       
     } catch(SQLException e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
       catch(Exception e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
     
       */
           
   }
   // -----------------------------------------------------------------
   //   ENVIO DE MENSAJE
   // -----------------------------------------------------------------
  
 //  public void envio(PrintStream os,byte[] buffer){
     public void envio(OutputStream salida,byte[] buffer){
       try{ 
        if(buffer != null ) 
                  salida.write(buffer);
       }catch (IOException e) {
            System.out.println("Fallo de envio con cliente.");            
            // System.exit(1);
        }
   }   
}
