/*
 *   M553 ARRANQUE / PARADA
 *   1-Marcha 2-Paro.
 */

package sockserver;


/**
 *
 * @author Jose
 */
public class M553 {
    private byte[] buffer;
    private int I = 0;
    
    public short cabecera;    
    public short linea;
    public short accion;
    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));     
      accion =   C.BytetoShort(Corte(2));
      
      // Mensaje 
      Concad();     
   }
   
   private void Concad(){
     MenStr="Mensaje ("+cabecera+")linea ("+linea+") accion ("+accion+")\n";                   
   }
   public byte[] encode(){
      Conv C = new Conv();
      C.AddShort(cabecera);
      C.AddShort(linea);
      C.AddShort(accion);      
      buffer = C.getBuffer();
      Concad();      
      return buffer;      
   }
    
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
}
