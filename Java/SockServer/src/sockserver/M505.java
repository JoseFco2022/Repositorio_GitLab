/*
 * M505.java
 * 
 * Created on 02-abr-2008, 13:58:08
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *   PEDIDOS DEL PLC QUE SE ENVIAN AL SGA
 * 
 */
public class M505 {
   byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short linea;
    public short mensaje;
    public short codigo;    
    public short pendiente;    
    public short cantidad;
    public short lote;
    public short amasado;
 //   private String samasado;
    
// -----
    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));      
      mensaje =  C.BytetoShort(Corte(2));
      codigo =  C.BytetoShort(Corte(2));
      pendiente =  C.BytetoShort(Corte(2));      
      cantidad =  C.BytetoShort(Corte(2));
      lote =  C.BytetoShort(Corte(2));      
      amasado = C.BytetoShort(Corte(2));

      Concad();      
    
   }
    
   private void Concad()
   {
  
      // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+")mensaje ("+mensaje+") codigo ("+codigo;
      MenStr=MenStr+") pendiente ("+pendiente+") cantidad ("+cantidad;
      MenStr=MenStr+") lote ("+lote+") masa ("+amasado+")\n";       
   }
   
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
}
