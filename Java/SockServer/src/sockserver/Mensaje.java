/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

/**
 *
 * @author jose
 */
public class Mensaje {
    byte[] buffer;
    int I = 0;
    public short cabecera;
    public short telegrama;
    public short lote;
    public short producto;
    public short barquetas;
    public String fecha;
    public short barq_caja;
    public short linea;
    public short peso_barqueta;
    public String ean_producto;
    public String ean_etiqueta;
    //  Fabricacion y Rechazo
    public short barq_fabricadas;
    public short barq_r_etiquetadora;
    public short barq_deteccion;
    public short barq_r_deteccion;
    public short barq_encajadora;
    public short barq_r_encajadora;
    //  Datos de Torre
    public String caja1;
    public short lote_caja1;
    public String caja2;
    public short lote_caja2;
    // Cambio de Lote
    public short camb_lote;
    public short camb_masa;
    public short camb_fabricada;
 //   public short camb_masa;

    // Mensaje conformado
    public String MenStr;
    

    public void Mensaje(){
        
    }
    
    public void decodifica(byte[] buf){
        Conv C = new Conv();
        buffer = buf;
        cabecera = C.BytetoShort(Corte(2));
        if(cabecera == 551) mensaje551();
        
        
    }
    
    
    private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }
    
    private void mensaje551(){
      Conv C = new Conv();  
      telegrama =  C.BytetoShort(Corte(2));
      lote =   C.BytetoShort(Corte(2));
      producto =   C.BytetoShort(Corte(2));
      barquetas =  C.BytetoShort(Corte(2));
      barq_caja =  C.BytetoShort(Corte(2));
      peso_barqueta =  C.BytetoShort(Corte(2));
      fecha = C.BytetoStr(Corte(12));
      ean_producto = C.BytetoStr(Corte(13));
      ean_etiqueta = C.BytetoStr(Corte(12)); 
      // Mensaje 
      MenStr="Mensaje ("+cabecera+")telegrama ("+telegrama+") Lote ("+lote+") Producto ("+producto;
      MenStr=MenStr+") Barquetas ("+barquetas+") Barq/Caja ("+barq_caja+") Peso ("+peso_barqueta+") Fecha ("+fecha;
      MenStr=MenStr+") Linea ("+linea+") Producto ("+ean_producto+") Etiqueta ("+ean_etiqueta+")";
    }
    
    private void mensaje501(){
      Conv C = new Conv();  
      telegrama =  C.BytetoShort(Corte(2));
      lote =   C.BytetoShort(Corte(2));
      producto =   C.BytetoShort(Corte(2));
      barquetas =  C.BytetoShort(Corte(2));
      barq_caja =  C.BytetoShort(Corte(2));
      peso_barqueta =  C.BytetoShort(Corte(2));
      fecha = C.BytetoStr(Corte(12));
      ean_producto = C.BytetoStr(Corte(13));
      ean_etiqueta = C.BytetoStr(Corte(12)); 
      // Mensaje 
      MenStr="Mensaje ("+cabecera+")telegrama ("+telegrama+") Lote ("+lote+") Producto ("+producto;
      MenStr=MenStr+") Barquetas ("+barquetas+") Barq/Caja ("+barq_caja+") Peso ("+peso_barqueta+") Fecha ("+fecha;
      MenStr=MenStr+") Linea ("+linea+") Producto ("+ean_producto+") Etiqueta ("+ean_etiqueta+")";
    }
    
}
