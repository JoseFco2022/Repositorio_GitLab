/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M503;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.*;
import java.io.*;
/**
 *
 * @author jose
 */
public class DM503 extends M503{
 
    DM503(byte[] buffer,Datos DB,Lineas L1,Lineas L2,Lineas L3,Lineas L4){
        
      decode(buffer);
      // Traspaso de Datos a Lineas
      /*  Ya no es necesario Actualizamos por el 501
       if(linea == 1) actualiza_datos(L1);
       if(linea == 2) actualiza_datos(L2);
       if(linea == 3) actualiza_datos(L3);
       if(linea == 4) actualiza_datos(L4);   */    
     // Graba datos en DB  . Se graba solo los datos de amasado anterior
    if(lote_ant > 0){
      
      int id = DB.ultimo("pedido_masa","id");
      try{          
          String Cad = "INSERT INTO pedido_masa (id,lote,masa,bpedido,bamasado,rvision,rmetal,rsellado,lote_ant,masa_ant,tipo_final) VALUES (?,?,?,?,?,?,?,?,?,?,?)";          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.setInt(1,id);
          C.setInt(2,lote);
          C.setInt(3,amasado);
          C.setInt(4,barquetas_pedido);        
          C.setInt(5,barquetas_amasado);
          C.setInt(6,barquetas_rvision);
          C.setInt(7,barquetas_rmetal);
          C.setInt(8,barquetas_rsellado);
          if(lote_ant > 0) C.setInt(9,lote_ant + 360000);  //TEST 22/08/12
          else C.setInt(9,lote_ant);
          
          if(amasado_ant > 0) C.setInt(10,amasado_ant + 360000);  //TEST 22/08/12
          else C.setInt(10,amasado_ant);
          
//28052009          C.setInt(9,lote_ant);
//28052009          C.setInt(10,amasado_ant);          
          C.setInt(11,tipo_final);          
          C.execute();
      } catch(SQLException e) {
                                System.out.println("Error insercion pedido_masa");
                                e.printStackTrace();                          
                                    }  
    }     
  /*    
      // Actualiza los datos de Masa y Lote
      try{          
          String Cad = "UPDATE linea SET lote=?,amasado=?" +
                "  WHERE linea ="+linea;          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.setInt(1,lote);
          C.setInt(2,amasado);
          C.execute();
      } catch(SQLException e) {
                                System.out.println("Error update linea");
                                e.printStackTrace();                          
                                    }   
    */   
         
    }
    
    private void actualiza_datos(Lineas L){
    //   L.lote = lote;
    //   L.amasado = amasado;
    }
}
