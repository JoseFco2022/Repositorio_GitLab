/*
 * M501.java
 * 
 * Created on 06-mar-2008, 12:13:40
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *
 * @author jose
 */
public class M501 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short linea;
    public short barquetas;    
    public short barquetas_RechazoTotal;    
    public short barquetas_Rechazovision;    
    public short barquetas_Rechazometal;    
    public short barquetas_Rechazosellado;    
    public short barquetas_Detec;
    public short barquetas_RDetec;
    public short barquetas_Encajadora;
    public short reserva;
    public short cajas;
    public short cajas_rechazo;
    public int lote;
    public int amasado;       
    public short peso_masa_resto;    
    // 0- Normal 1- Alarma  3- Normal no actualizado 4- Alarma no actualizado
    public int[] A = new int[16];
// -----
    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      // Linea 1
      linea =  C.BytetoShort(Corte(2));
      barquetas =  C.BytetoShort(Corte(2));
      barquetas_RechazoTotal =  C.BytetoShort(Corte(2));
      barquetas_Rechazovision =  C.BytetoShort(Corte(2));      
      barquetas_Rechazometal =  C.BytetoShort(Corte(2));
      barquetas_Rechazosellado =  C.BytetoShort(Corte(2));      
      barquetas_Detec =  C.BytetoShort(Corte(2));     
      barquetas_Encajadora =  C.BytetoShort(Corte(2));      
      cajas =  C.BytetoShort(Corte(2));
      cajas_rechazo =  C.BytetoShort(Corte(2));
      lote =  C.BytetoShort(Corte(2));
      amasado =  C.BytetoShort(Corte(2));
      peso_masa_resto =  C.BytetoShort(Corte(2));
      byte int_estado =Corte_Byte(); //B28
      A[0]=Estado_Alarma(int_estado,A[0],1,C);      
      int_estado = Corte_Byte();         //B29
      A[1]=Estado_Alarma(int_estado,A[1],1,C);
      A[2]=Estado_Alarma(int_estado,A[2],4,C);
      A[3]=Estado_Alarma(int_estado,A[3],7,C);      
      int_estado = Corte_Byte();         //B30
      A[4]=Estado_Alarma(int_estado,A[4],2,C);
      A[5]=Estado_Alarma(int_estado,A[5],5,C);
      int_estado = Corte_Byte();         //B31
      A[6]=Estado_Alarma(int_estado,A[6],0,C);
      A[7]=Estado_Alarma(int_estado,A[7],6,C);      
      int_estado = Corte_Byte();         //B32
      A[8]=Estado_Alarma(int_estado,A[8],1,C);
      int_estado = Corte_Byte();         //B33
      A[9]=Estado_Alarma(int_estado,A[9],0,C);
      A[10]=Estado_Alarma(int_estado,A[10],4,C); 
      A[11]=Estado_Alarma(int_estado,A[11],7,C); 
      int_estado = Corte_Byte();         //B34
      A[12]=Estado_Alarma(int_estado,A[12],0,C);
      A[13]=Estado_Alarma(int_estado,A[13],1,C); 
      A[14]=Estado_Alarma(int_estado,A[14],2,C);       
      A[15]=Estado_Alarma(int_estado,A[15],3,C);       
      
      Concad();
     
   }
  
   private int Estado_Alarma(int int_estado,int A,int posicion,Conv C){
       
       // Se ha producido un estado de alarma
       if(C.IsActive(int_estado,posicion)){
           if(A == 0) A = 3;  // Se activa la alarma
       }
       else{
           if(A == 1) A = 2;  // Se desactiva la alarma
       }
       return A;
   }
   
   // -------------------------------------------
   private void Concad(){
        // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+")pedido ("+lote+")masa ("+amasado+") masa final ("+peso_masa_resto+")barquetas ("+barquetas+") b.rechazo ("+barquetas_RechazoTotal+") Deteccion ("+barquetas_Detec;
      MenStr=MenStr+") Rechazo vision. ("+barquetas_Rechazovision+") Rechazo metal. ("+barquetas_Rechazometal+") Rechazo sellado. ("+barquetas_Rechazosellado+") Encajadora ("+barquetas_Encajadora ;
      MenStr=MenStr+") Rechazo caja ("+cajas_rechazo+") Cajas ("+cajas+")\n";        
    
   }
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
     private byte Corte_Byte(){
//        byte bt = new byte[x];
        
           byte bt = buffer[I]; I++;
        
        return bt;
    } 
}
