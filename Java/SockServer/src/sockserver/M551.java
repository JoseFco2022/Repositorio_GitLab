/*
 * M551.java
 * 
 * Created on 06-mar-2008, 11:44:46
 * 
 *   M551 ENVIO DATOS PEDIDO
 * 
 */

package sockserver;



/**
 *
 * @author jose
 */
public class M551 {
    private byte[] buffer;
    private int I = 0;
    
    public short cabecera;
    public short telegrama;
    public short lote;
    public short producto;
    public short barquetas;
    public String fecha;
    public short barq_caja;
    public short linea;
    public String fin_pedido;
    public short peso_barqueta;
    public String ean_producto;
    public String ean_etiqueta;
    public short retirada;
    // Mensaje conformado
    public String MenStr;    
    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      telegrama =  C.BytetoShort(Corte(2));
      lote =   C.BytetoShort(Corte(2));
      producto =   C.BytetoShort(Corte(2));
      barquetas =  C.BytetoShort(Corte(2));
      barq_caja =  C.BytetoShort(Corte(2));
      peso_barqueta =  C.BytetoShort(Corte(2));
      fecha = C.BytetoStr(Corte(12));
      linea = C.BytetoShort(Corte(2));
      ean_producto = C.BytetoStr(Corte(13));
      Corte(1);
      ean_etiqueta = C.BytetoStr(Corte(8)); 
      fin_pedido = C.BytetoStr(Corte(1));
      Corte(1);
      retirada = C.BytetoShort(Corte(2));
      
      
      Concad();
      // Mensaje 
    
   }
   
   public byte[] encode(){
      Conv C = new Conv();
      if(fin_pedido == null) fin_pedido="N";
      C.AddShort((short)551);
      C.AddShort(telegrama);
      C.AddShort(lote);
      C.AddShort(producto);
      C.AddShort(barquetas);
      C.AddShort(barq_caja);      
      C.AddShort(peso_barqueta);
      C.AddString(fecha,12);
      C.AddShort(linea);
      C.AddString(ean_producto,13);
      C.AddString(" ",1);
      C.AddString(ean_etiqueta,8); 
      C.AddString(fin_pedido,1);
      C.AddString(" ",1);
      C.AddShort(retirada);      
      C.AddString(" ",12);
      buffer = C.getBuffer();
      Concad();
      return buffer;      
   }
    
   private void Concad(){
      MenStr="Mensaje ("+cabecera+")telegrama ("+telegrama+") Lote ("+lote+") Producto ("+producto;
      MenStr=MenStr+") Barquetas ("+barquetas+") Barq/Caja ("+barq_caja+") Peso ("+peso_barqueta+") Fecha ("+fecha;
      MenStr=MenStr+") Retirada ("+retirada+") Linea ("+linea+") Producto ("+ean_producto+") Fin Pedido ("+fin_pedido+") Etiqueta ("+ean_etiqueta+")\n";            
   }
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
}
