/*
 * M503.java
 * 
 * Created on 06-mar-2008, 12:46:45
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *
 * @author jose
 */
public class M503 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short lote;
    public short linea;
    public short amasado;
    public short tipo_final;  // 1- Masa 2- Pedido 3- Todos
    public short barquetas_amasado;
    public short barquetas_pedido;
    public short lote_ant;
    public short amasado_ant;   
    public String res2;
    public short barquetas_rvision;
    public short barquetas_rmetal;
    public short barquetas_rsellado;
    public short barquetas_rechazo;
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));
      lote =  C.BytetoShort(Corte(2));
      amasado =  C.BytetoShort(Corte(2));
      tipo_final =  C.BytetoShort(Corte(2));
      lote_ant =  C.BytetoShort(Corte(2));
      amasado_ant =  C.BytetoShort(Corte(2));      
      res2 =  C.BytetoStr(Corte(2));
      barquetas_pedido = C.BytetoShort(Corte(2));
      barquetas_rvision =  C.BytetoShort(Corte(2));
      barquetas_rmetal =  C.BytetoShort(Corte(2));
      barquetas_rsellado = C.BytetoShort(Corte(2));
      barquetas_amasado = C.BytetoShort(Corte(2));
      


      // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+")Tipo_fin ("+tipo_final+")ID.lote ("+lote_ant+") ID.amasado ("+amasado_ant;
      MenStr=MenStr+") barquetas pedido ("+barquetas_pedido+") barquetas amasado ("+barquetas_amasado+") rec.vision ("+barquetas_rvision ;
      MenStr=MenStr+") rec.metal ("+barquetas_rmetal+") rec.sellado ("+barquetas_rsellado+")";        
      MenStr=MenStr+"\n";    
   }
    
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
}
