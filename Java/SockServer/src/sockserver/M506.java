/*
 * M502.java
 * 
 * Created on 06-mar-2008, 12:29:56
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *
 * @author jose
 */
public class M506 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public String caja1;
    public String caja2;
    public String caja3;
    public String caja4;
    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      caja1 =  C.BytetoStr(Corte(20));
      caja2 =  C.BytetoStr(Corte(20));
      caja3 =  C.BytetoStr(Corte(20));
      caja4 =  C.BytetoStr(Corte(20));      
      
      if(caja1.contains("\u0000\u0000\u0000\u0000\u0000")) 
            caja1="#";
      if(caja2.contains("\u0000\u0000\u0000\u0000\u0000")) 
            caja2="#";
      if(caja3.contains("\u0000\u0000\u0000\u0000\u0000")) 
            caja3="#";
      if(caja4.contains("\u0000\u0000\u0000\u0000\u0000")) 
            caja4="#";      
      
      Concad();
    
   }
   
   
      public byte[] encode(){
      Conv C = new Conv();
      C.AddShort(cabecera);
      C.AddString(caja1,20);
      C.AddString(caja2,20);
      C.AddString(caja3,20);
      C.AddString(caja4,20);

      buffer = C.getBuffer();
      Concad();
      return buffer;      
   }

   private void Concad(){
             // Mensaje 
      MenStr="Mensaje ("+cabecera+")caja1 ("+caja1+")caja2 ("+caja2+")caja3 ("+caja3+")\n";              
   }  
   
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
}
