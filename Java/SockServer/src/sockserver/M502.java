/*
 * M502.java
 * 
 * Created on 06-mar-2008, 12:29:56
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;


/**
 *
 * @author jose
 */
public class M502 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short linea;
    public String caja1;
    public short mensaje1;
    public short barquetas1;
    public String caja2;
    public short mensaje2;
    public short barquetas2;
    public String reserva;
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea = C.BytetoShort(Corte(2));
      caja1 =  C.BytetoStr(Corte(20));
//      reserva = C.BytetoStr(Corte(1));
      mensaje1 =  C.BytetoShort(Corte(2));
//      reserva = C.BytetoStr(Corte(2));
      barquetas1 = C.BytetoShort(Corte(2));
      caja2 =  C.BytetoStr(Corte(20));
//      reserva = C.BytetoStr(Corte(1));
      mensaje2 = C.BytetoShort(Corte(2));
//      reserva = C.BytetoStr(Corte(2));
      barquetas2 = C.BytetoShort(Corte(2));
      
      if(caja1.contains("\u0000\u0000\u0000\u0000\u0000")) 
            caja1="#";
          
      Concad();
    
   }
   
   
      public byte[] encode(){
      Conv C = new Conv();
      C.AddShort(cabecera);
      C.AddShort(linea);
      C.AddString(caja1,20);
      C.AddString(" ", 1);
      C.AddShort(mensaje1);
      C.AddString("  ", 2);
      C.AddShort(barquetas1);
      C.AddString(caja2,20);
      C.AddString(" ", 1);
      C.AddShort(mensaje2);
      C.AddString("  ", 2);
      C.AddShort(barquetas2);
      buffer = C.getBuffer();
      Concad();
      return buffer;      
   }

   private void Concad(){
             // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+")caja1 ("+caja1+") mensaje1 ("+mensaje1;
      MenStr=MenStr+") barquetas1 ("+barquetas1+")\n";              
   }  
   
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
    
}
