package sockserver;

import java.sql.*;
import java.io.*;


public class BaseDatos {

    private String driver;
    private String url;
    private String login;
    private String passwd;
    public  Connection con = null;     
    private Statement sql = null;
    public ResultSet res;

    public BaseDatos (String base, String _url, String _login, String _passwd) {
       base.toLowerCase(); 
       if(base.equals("interbase")) driver = "org.firebirdsql.jdbc.FBDriver";
       else if(base.equals("oracle")) driver = "oracle.jdbc.driver.OracleDriver";
       else if(base.equals("sqlserver")) driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
       else System.out.println("Error el driver no está definido");              
       url=_url; login=_login; passwd=_passwd; 
    }

    /**
     *  -------------------------------------------------------------------------
     *       APERTURA DE LA BASE DE DATOS
     *  -------------------------------------------------------------------------
     */
    public void Open () {
      try {        
        Class.forName(driver).newInstance();
       //   Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        } catch (Exception e) {
                            System.out.println("No se pudo cargar el driver "+driver);
                            e.printStackTrace();
                            return;
                              }
           //Establish a connection
      try {

     //    con = DriverManager.getConnection(url, login, passwd);
         con = DriverManager.getConnection(url,login,passwd);
         sql = con.createStatement();
         }catch (SQLException sqle) {
                                System.out.println("Error con la conexión a la base de datos:");
                                sqle.printStackTrace();
                                    }         
 
      
    }
    
// -------------------------------------------------------------------------
//      EJECUTA CONSULTA
// -------------------------------------------------------------------------    
 public ResultSet ejecuta_consulta(String consulta)   
 {
     Statement sql1 = null; 
     ResultSet R = null;
     try
     {
      sql1 = con.createStatement();   
      R = sql1.executeQuery(consulta); 
   //     res.next();
     } catch(Exception e) {
         System.out.println("Error de consulta");
         return null;
           }
        
    return R;
 }
    
 
 // -------------------------------------------------------------------------
//      PREPARA COMANDO
// -------------------------------------------------------------------------    
 public PreparedStatement prepara_comando(String comando)   
 {
     PreparedStatement ps = null;
     try
     {
        ps=con.prepareStatement(comando);                  
     } catch(Exception e) {
         System.out.println("Error preparacion comando");
         e.printStackTrace();
         return null;
           }
        
    return ps;
 }
 // -------------------------------------------------------------------------
//      EJECUTA COMANDO
// -------------------------------------------------------------------------    

   public boolean comando(String orden){
       Statement sentencia = null;
     
       
        try{
            sentencia=con.createStatement(); 
            sentencia.execute(orden);
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
        
    }
  
//  -------------------------------------------------------------------------
//       VALOR GENERADOR
//  -------------------------------------------------------------------------     
  public Integer NextVal(String generador) 
    {
      try {
          ResultSet Q = ejecuta_consulta("SELECT GEN_ID("+generador+",1) FROM RDB$DATABASE");
          Integer I = Q.getInt(1);
          return I;
          } catch (SQLException sqle) {
                                        sqle.printStackTrace();
                                        return 0;
                                        } 
    }
    
  
  //  -------------------------------------------------------------------------
//       VALOR GENERADOR
//  -------------------------------------------------------------------------     
  public void Close() 
    {
      try {
           sql.close();
           con.close();
          } catch (SQLException sqle) {
                                        sqle.printStackTrace();                                        
                                        } 
    }

}

