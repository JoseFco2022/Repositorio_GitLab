/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M552;
import java.sql.*;
/**
 *
 * @author Jose
 */
public class DM552 extends M552 {
    public byte[] buffer;
    
    public void graba_masa(byte[] buffer1,Datos DB){
        // Decodificar
        decode(buffer1);
        // guardar los datos en nuestra DB
     // Graba datos en DB 
      int id = DB.ultimo("masa","id");
 //     int umensaje = DB.ultimo("mensaje","mensaje");
      try{          
          String Cad = "INSERT INTO masa (id,numero,codigo,linea,lote,cantidad,fabricado) " +
                " VALUES (?,?,?,?,?,?,?)";          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.setInt(1,id);
          C.setInt(2,id);
   //       C.setInt(3,umensaje);                    
          C.setInt(3,producto);        
          C.setInt(4,linea);
          C.setInt(5,Integer.parseInt(amasado));
          C.setInt(6,peso_masa);
          C.setInt(7,0);
          C.execute();
          
      } catch(SQLException e) {
                                System.out.println("Error insercion pedido_masa");
                                e.printStackTrace();                          
                                    }         
     // Preparar los Mensajes para el envio
       try{          
          String Cad = "execute envia_masas";          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.execute();
      } catch(SQLException e) {
                                System.out.println("Error creación mensajes");
                                e.printStackTrace();                          
                                    } 
      
    }
    
    // Se lee de la BD y se le envia a siemens
    public boolean CreaMensaje(ResultSet Q){
      boolean existe = false;  
      try{               
       if (Q.next()){
        cabecera=552;
        linea=Q.getShort("linea");
        peso_masa=Q.getShort("cantidad");
        producto=Q.getShort("codigo");
        telegrama=(short)(Q.getInt("mensaje")-360000); //TEST 22/08/12
//28052009        telegrama=Q.getShort("mensaje");
        amasado=Q.getString("lote");
        int L = amasado.length();
        if(L == 1) amasado="000"+amasado; 
        if(L == 2) amasado="00"+amasado; 
        if(L == 3) amasado="0"+amasado; 
        barquetas=0;  //Poner a 0 reservado.
        buffer=encode();
//        byte[] buffer = encode();
    
           }
      } catch(SQLException e) {
                                System.out.println("Error en consulta");
                                e.printStackTrace();                                
                                    }
      return existe; 
    }
    
   
    
}
