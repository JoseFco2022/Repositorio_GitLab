/*
 * M504.java
 * 
 * Created on 06-mar-2008, 13:50:20
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;



/**
 *
 * @author jose
 */
public class M504 {
    byte[] buffer;
    int I = 0;
    
    public short cabecera;
    public short linea;
    public short codigo;
    public short parametro;    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));
      codigo =  C.BytetoShort(Corte(2));
      parametro =  C.BytetoShort(Corte(2));
      
      Concad(); 
   }
    
   private void Concad(){
      MenStr="Mensaje ("+cabecera+")linea ("+linea+") codigo ("+codigo;
      MenStr=MenStr+") parametro ("+parametro+")\n";
   }
   
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
}
