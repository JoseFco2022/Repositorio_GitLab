/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M500;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.*;
import java.io.*;


public class DM500 extends M500{
    
   DM500(byte[] buffer,Datos DB){

     try{  
      decode(buffer);
      int id = DB.ultimo("masa","id");
      
      Conversion X = new Conversion();
      String Cad="INSERT INTO masa (id,numero,codigo,linea,lote,cantidad,fabricado)" +
              " VALUES (?,?,?,?,?,?,?)";
      PreparedStatement C = DB.prepara_comando(Cad);
      C.setInt(1,id);
      C.setInt(2,id);
      C.setInt(3,receta);
      C.setInt(4,linea);      
      C.setInt(5,amasado);
      C.setInt(6,peso_masa);
      C.setInt(7,0);      
      C.executeUpdate();
      C.close();
     }catch(SQLException e) {
                                System.out.println("Error inserción de masa");                                
                                e.printStackTrace();
                                    } 
     
     try{             
       PreparedStatement C1 = DB.prepara_comando("execute envia_masas");
       C1.execute();
       C1.close();       
     }catch(SQLException e1) {
                                System.out.println("Error inserción de masa");                                
                                e1.printStackTrace();                      
                                    } 
     
   }
}
