/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

/**
 *
 * @author jose
 */
public class Lineas {
  public int lote;
  public int linea;
  public int amasado;
  public int barqueta_pedido;
  public int peso_masa;
  public int consumo_masa;
  public int barqueta_rechazo;
  public int barqueta_deteccion;
  public int barqueta_rechazo_metal;
  public int barqueta_rechazo_vision;
  public int barqueta_rechazo_sellado;
  public int barqueta_encajadora;
  public int rechazo_cajas;
  public int cajas;  
  public int lote_anterior;
  public int amasado_anterior;  
  public int estado;
  public boolean masa_inicio_etiq;    // Indica si se ha grabado el inicio_etiq de la masa
  public boolean lote_inicio_etiq;    // Indica si se ha grabado el inicio_etiq de pedido


public String encode(){
  String cad = "lote"+lote+")b_pedido"+barqueta_pedido+")amasado"+amasado+")peso_masa"+peso_masa;
  cad+=")consumo_masa"+consumo_masa+")b_rechazo"+barqueta_rechazo;
  cad+=")b_rechazo_m"+barqueta_rechazo_metal+")b_rechazo_v"+barqueta_rechazo_vision;
  cad+=")b_rechazo_s"+barqueta_rechazo_sellado+")b_encajadora"+barqueta_encajadora;
  cad+=")rechazo_cajas"+rechazo_cajas+")tcajas"+cajas+")\n";
  return cad;
}

public void ver_datos(){
  System.out.println("Linea :"+linea+" lote:"+lote+" amasado:"+amasado+" b.pedido:"+barqueta_pedido+" b.rechazo(m):"+barqueta_rechazo_metal+" cajas:"+cajas);  
}

}
