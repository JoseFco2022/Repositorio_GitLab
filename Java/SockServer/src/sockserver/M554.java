/*
 * M554   ACCION A REALIZAR ANTE CUALQUIER EVENTO.
 * 
 */

package sockserver;



/**
 *
 * @author Jose
 */
public class M554 {
    private byte[] buffer;
    private int I = 0;
    
    public short cabecera;    
    public short linea;
    public short codigo;
    public short parametro;    
    
    // Mensaje conformado
    public String MenStr;    
    
    
   public void decode(byte[] buf){
      Conv C = new Conv();
      buffer = buf;
        
      cabecera = C.BytetoShort(Corte(2));
      linea =  C.BytetoShort(Corte(2));     
      codigo =   C.BytetoShort(Corte(2));
      parametro =   C.BytetoShort(Corte(2));
      cabecera = 554;
      // Mensaje 
      MenStr="Mensaje ("+cabecera+")linea ("+linea+") codigo ("+codigo+") parametro ("+parametro+")\n";            
   }
   
   public byte[] encode(){
      Conv C = new Conv();
      C.AddShort(cabecera);
      C.AddShort(linea);
      C.AddShort(codigo);      
      C.AddShort(parametro);      
      buffer = C.getBuffer();
      decode(buffer);
      return buffer;       
   }
    
   private byte[] Corte(int x){
        byte[] bt = new byte[x];
        for(int i=0; i < x; i++ ) {
            bt[i] = buffer[I]; I++;
        }
        return bt;
    }  
}
