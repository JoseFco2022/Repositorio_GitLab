/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import java.net.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author jose
 * Clase para a recepción de datos de Clientes
 */
public class MutiSocket extends Thread{
    private Socket clientSocket = null;
    private PrintWriter out;
    private PrintStream os;
    private InputStreamReader isr;
    private InputStream entrada;
    private OutputStream salida;
    private BufferedReader in;
    private Lineas L1,L2,L3,L4;
    private Datos DB;
    private String Ruta_Base;
    private String User;
    private String pass;
    private boolean grabacion;
    private boolean silencio;
    private int ciclo_grabacion;
//    private ClienteCom Com;
    
    MutiSocket(Socket socket,Lineas L1,Lineas L2,Lineas L3,Lineas L4,Config Conf){
        super("MultiSocket");
        this.clientSocket = socket;        
        this.L1 = L1; this.L2 = L2; this.L3 = L3; this.L4 = L4;
        this.Ruta_Base = Conf.ruta_base;
        this.User = Conf.user;
        this.pass = Conf.password;
        this.grabacion = Conf.grabacion;
        this.silencio = Conf.silencio;
        this.ciclo_grabacion = Conf.ciclo_grabacion;
        
      //  this.Com = Com;
    }
    
    public void run(){
     // Fichero para grabación de datos recibidos
       SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy h:mm a"); 
       ByteArrayOutputStream byteStr = new ByteArrayOutputStream();
       int i =0;
       boolean borra_plc = false;
       int linea_plc = 0;
       Tlog log = new Tlog();
    /*   
       FileWriter fichero  = null;
       PrintStream pw = null;  
       try{
          fichero = new FileWriter("log");
          pw = new PrintStream(fichero);
        }catch (Exception e){
            grabacion=false;
            e.printStackTrace();
        } */
       
   //  this.DB = new Datos("sqlserver","jdbc:sqlserver://localhost:1130;Database=DATOS", "SYSDBA","masterkey");         
      this.DB = new Datos("sqlserver",Ruta_Base,User,pass);                 
     
     try {
     

        // Abrir Stream Socket
        this.out = new PrintWriter(clientSocket.getOutputStream(), true);
        this.os = new PrintStream(clientSocket.getOutputStream());   
//        this.isr = new InputStreamReader(clientSocket.getInputStream());
        this.entrada = clientSocket.getInputStream();
        this.salida = clientSocket.getOutputStream();
//        this.in = new BufferedReader(isr);
        String inputLine, outputLine;

        byte[] buffer = new byte[160];
        
/*  
        int r =0;
        r = in.read();
           for(int I = 0; (I < 160) and (r != "\n"); I++)
               buffer[I]=(byte)r;
               r = in.read();               
*/           
           Conv C = new Conv();
          // String cad;       
          // while((cad = C.lee_buffer(in)) != null){
        //   int l = in.
       //     while((buffer = C.lee_buffer(in)) != null){
          while((buffer = C.lee_buffer(entrada)) != null){
           
           Date d = new Date();   
           String fecha=sdf.format(d);   
           // Presentacion de datos  
           if(grabacion) {
                 byteStr.write(buffer);
                 i++;
                 if(i == ciclo_grabacion){
                     i=0;
                     OutputStream fo = new FileOutputStream("log");
                     byteStr.writeTo(fo);
                     fo.close();                       
                     log.graba_log("Grabación finalizada.");}
                 }
           
         //  buffer = C.StrToByte(cad,150);
         //  short cab = C.BytetoShort(buffer);
             int cab = C.BytetoShort(buffer);
            
           if(!silencio) log.graba_log(" Mensaje "+Integer.toString(cab));
              
            // 53
           if(cab == 12341){      // Masa recibida por Carnitech
               //M551 M = new M551(buffer);
               int r = entrada.read();
               DM500 M = new DM500(buffer,DB);
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           } 
             
           if(cab == 501){       // Indicacion de barquetas/cajas producidas/rechazadas cada 10 segs
               DM501 M = new DM501(buffer,DB,L1,L2,L3,L4);               
               if(!silencio) log.graba_log(M.MenStr);
               if(silencio) L1.ver_datos();               
               // Enviar todos los datos pendientes y masas
             //  DB.Envio_mensajes(this.os);
                 DB.Envio_mensajes(this.salida);
                 
               // Envio de mensaje de datos en PLC
               M554 Plc = new M554();
               Plc.cabecera=554;
               Plc.linea=M.linea;
               Plc.codigo=3;
               Plc.parametro=0;   
               byte[] bufferPlc = Plc.encode();
               DB.envio(salida,bufferPlc);
               // Fin de envio
               linea_plc=M.linea;
               borra_plc=true;
           }           
           if(cab == 502){      // Torres enviadas a TGW  (escribir en la BD)  
               //M551 M = new M551(buffer);
               DM502 M = new DM502(buffer,DB);               
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           }           
           if(cab == 503){     //  Cambio de Lote por trazabilidad 
               //M551 M = new M551(buffer);
               DM503 M = new DM503(buffer,DB,L1,L2,L3,L4);
              // M.decode(buffer);  
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           }
           if(cab == 504){     // Evento producido
               //M551 M = new M551(buffer);
               DM504 M = new DM504(buffer,DB,L1,L2,L3,L4);
               // M.decode(buffer);  
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           }
           if(cab == 505){      // Pedidos contenidos en el PLC
               if(borra_plc){
                 borra_plc=false;
                 DB.comando("DELETE FROM plc WHERE linea = "+linea_plc);
                 }
               //M551 M = new M551(buffer);
               DM505 M = new DM505(buffer,DB);
               M.decode(buffer);  
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           }    
             
          if(cab == 506){      // Torres completas leidas 
               //M551 M = new M551(buffer);
               DM506 M = new DM506(buffer,DB);               
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,0,M.MenStr);
           }               

          if(cab == 507){      // Torres completas leidas 
               //M551 M = new M551(buffer);
               DM507 M = new DM507(buffer,DB);               
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,0,M.MenStr);
           }
             
           if(cab == 552){      // Masa procedente de Carnitek
               //M551 M = new M551(buffer);
               DM552 M = new DM552();
               // Grabar los datos de Carnitek
               M.graba_masa(buffer, DB);
  
               if(!silencio) log.graba_log(M.MenStr);
               DB.informe(cab,M.linea,M.MenStr);
           }                 
             
           if(cab == 562){      // Datos de la Linea enviado a Delphi
               //M551 M = new M551(buffer);
               String res=null;
               M601 M = new M601();
               M.decode(buffer);
               if(M.linea == 1) res=L1.encode();
               if(M.linea == 2) res=L2.encode();
               if(M.linea == 3) res=L3.encode();
               if(M.linea == 4) res=L4.encode();
              // out.write(res);
               envio(C.StrToByte(res,150));
        //       os.println(res);
               if(!silencio) {log.graba_log(M.MenStr);
               log.graba_log(res);}
           }           
           
           }

        byteStr.close();
        
        }catch (IOException e) {
            log.graba_log("Fallo de conexión con cliente.");            
        //    System.exit(1);                
        }      
     
      this.DB.Close(); 
    }
    
   public void envio(byte[] buffer){
       try{ 
        this.os.write(buffer);
       }catch (IOException e) {
            System.out.println("Fallo de envio con cliente.");            
            // System.exit(1);
        }
   }

   private void grabacion_log(PrintStream pw){
       
   }
}
