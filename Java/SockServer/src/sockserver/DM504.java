/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M504;
import java.sql.PreparedStatement;
import java.util.Date;
import java.sql.*;
import java.io.*;
/**
 *
 * @author jose
 */
public class DM504 extends M504 {
    
    DM504(byte[] buffer,Datos DB,Lineas L1,Lineas L2,Lineas L3,Lineas L4){
        
      decode(buffer);
      // Traspaso de Datos a Lineas
       if(linea == 1) actualiza_datos(L1);
       if(linea == 2) actualiza_datos(L2);
       if(linea == 3) actualiza_datos(L3);
       if(linea == 4) actualiza_datos(L4);       
     // Graba datos en DB  . Se graba solo los datos de amasado anterior
      
       try{          
          String Cad = "UPDATE linea SET estado=?" +
                "  WHERE linea ="+linea;          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.setInt(1,codigo);
          C.execute();
      } catch(SQLException e) {
                                System.out.println("Error update linea");
                                e.printStackTrace();                          
                                    }

    }
    
    private void actualiza_datos(Lineas L){
       L.estado = codigo;       
    }
    

}
