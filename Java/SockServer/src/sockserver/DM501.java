/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sockserver;

import sockserver.M501;
import java.sql.*;

/**
 *
 * @author jose
 */
public class DM501 extends M501{
    
    DM501(byte[] buffer,Datos DB,Lineas L1,Lineas L2,Lineas L3,Lineas L4){
       decode(buffer);     
       
       if(linea == 1) actualiza_datos(L1,DB);
       if(linea == 2) actualiza_datos(L2,DB);
       if(linea == 3) actualiza_datos(L3,DB);
       if(linea == 4) actualiza_datos(L4,DB);  
       
       // Grabar los datos en la BD
       graba_datos(linea,DB);  
    }
    private void actualiza_datos(Lineas L,Datos DB){

        // Grabación de datos en pedido_lote_linea si se cumple condiciones
        // antes de realizar las modificaciones de las variables
        // del objeto L
       graba_pedido_masa_linea(L,DB); 
                

       
       // Actualiza variable objeto lineas
       if (L.lote != lote) L.lote_inicio_etiq = false;
       if (L.amasado != amasado) L.masa_inicio_etiq = false;
       
       L.linea = linea; 
       L.barqueta_pedido = barquetas;
       L.barqueta_deteccion = barquetas_Detec;
       L.barqueta_encajadora = barquetas_Encajadora;
       L.barqueta_rechazo = barquetas_RechazoTotal;
       L.barqueta_rechazo_metal = barquetas_Rechazometal;
       L.barqueta_rechazo_sellado = barquetas_Rechazosellado;
       L.barqueta_rechazo_vision = barquetas_Rechazovision;
       L.cajas = cajas;
       L.rechazo_cajas = cajas_rechazo;
       L.lote = lote;
       L.amasado = amasado;
       
      graba_inicio_fin(L,DB);
    }

    // Su función es grabar en la tabla pedido_masa_linea 
    // cuando se produce un cambio de pedido
    private void graba_pedido_masa_linea(Lineas L,Datos DB)
    {
        // Grabar los datos finalizacion pedido o amasado  
      
      if ((amasado != L.amasado) || (lote != L.lote))
      {
      int id = DB.ultimo("pedido_masa_lin","id");
      
      try{          
          String Cad = "INSERT INTO pedido_masa_lin (id,linea,lote,amasado," +
                  "brechazo_m,brechazo_v," +
                  "brechazo_s,bencajadora,bpedido) VALUES (?,?,?,?,?,?,?,?,?)";
          
          PreparedStatement C = DB.prepara_comando(Cad);
          
          
          C.setInt(1,id);
          C.setInt(2,L.linea);
          
          if (L.lote > 0 )   C.setInt(3,L.lote + 360000);  //TEST 22/08/12
          else C.setInt(3,0);
          
          if(L.amasado > 0) C.setInt(4,L.amasado + 360000);  //TEST 22/08/12
          else C.setInt(4,0);
          
          C.setInt(5,L.barqueta_rechazo_metal);                    
          C.setInt(6,L.barqueta_rechazo_vision);          
          C.setInt(7,L.barqueta_rechazo_sellado);
          C.setInt(8,L.barqueta_encajadora);
          C.setInt(9,L.barqueta_pedido);          
          C.execute(); 
      } catch(SQLException e) {
                                System.out.println("Error inserción pedido_masa_lin");
                                e.printStackTrace();                          
                                    } 
      }
      
    }
    //
    // Poner los tiempos de inicio y finalizacion de masa
    // iniciamos cuando barquetas > 0
    // finalizamos cuando amasado != L.amasado
    //
        private void graba_inicio_fin(Lineas L,Datos DB)
    {
            
            
        // Grabar los datos inicio amasado  
      
      if ((L.masa_inicio_etiq = false) && (L.barqueta_pedido > 0))
      {
            
      try{          
          String Cad = "EXEC DB501_inicio_fin ?,?";
          
          PreparedStatement C = DB.prepara_comando(Cad);
          
          
          C.setInt(1,2);  // Tipo 2 es la masa
          
          if (L.amasado > 0 )
          {
              C.setInt(2,L.amasado + 360000);    //TEST 22/08/12
              C.execute();               
          }
          
          L.masa_inicio_etiq = true;
          
      } catch(SQLException e) {
                                System.out.println("Error EXEC DB501_inicio_fin ?,?");
                                e.printStackTrace();                          
                                    } 
      }
      
      // Grabar los datos inicio pedido 
      
      if ((L.lote_inicio_etiq = false) && (L.barqueta_pedido > 0))
      {
            
      try{          
          String Cad = "EXEC DB501_inicio_fin ?,?";
          
          PreparedStatement C = DB.prepara_comando(Cad);
          
          
          C.setInt(1,1);  // Tipo 1 es el pedido
          
          if (L.lote > 0 )
          {
              C.setInt(2,L.lote + 360000);    //TEST 22/08/12
              C.execute();               
          }
          
          L.lote_inicio_etiq = true;
          
      } catch(SQLException e) {
                                System.out.println("Error EXEC DB501_inicio_fin ?,?");
                                e.printStackTrace();                          
                                    } 
      }      
      
      
    }
    
    
    public void graba_datos(int linea,Datos DB){
      try{          
          String Cad = "UPDATE linea SET bpedido=?,bdeteccion=?,brechazo_m=?,brechazo_v=?,brechazo_s=?,bencajadora=?,rechazo_caja=?,caja=?,lote=?,amasado=?,peso_masa_resto=? " +
                ",A1=?,A2=?,A3=?,A4=?,A5=?,A6=?,A7=?,A8=?,A9=?,A10=?,A11=?,A12=?,A13=?,A14=?,A15=?,A16=? " +  
                "  WHERE linea ="+linea;          
          PreparedStatement C = DB.prepara_comando(Cad);
          C.setInt(1,barquetas);
          C.setInt(2,barquetas_Detec);
          C.setInt(3,barquetas_Rechazometal);                    
          C.setInt(4,barquetas_Rechazovision);          
          C.setInt(5,barquetas_Rechazosellado);
          C.setInt(6,barquetas_Encajadora);
          C.setInt(7,cajas_rechazo);
          C.setInt(8,cajas);
          if(lote > 0 ) C.setInt(9,lote + 360000);  //TEST 22/08/12
          else C.setInt(9,lote);          
//28052009          C.setInt(9,lote);
          
          if(amasado > 0 ) C.setInt(10,amasado + 360000);  //TEST 22/08/12
          else C.setInt(10,amasado);
                  
//28052009          C.setInt(10,amasado);
          C.setInt(11,peso_masa_resto); 
          for(int i=0; i < 16; i++ ) {
              C.setInt(i+12,A[i]);
              if(A[i] > 1 ) A[i] = A[i] -2;
          }

          
          
          C.execute();
      } catch(SQLException e) {
                                System.out.println("Error update linea");
                                e.printStackTrace();                          
                                    }      
      
     
      
      
    }
    
    
    
}
